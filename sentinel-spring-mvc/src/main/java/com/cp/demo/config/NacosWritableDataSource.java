package com.cp.demo.config;


import com.alibaba.csp.sentinel.datasource.WritableDataSource;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigFactory;
import com.alibaba.nacos.api.config.ConfigService;

import java.util.Properties;

/**
 * sentinel配置动态推送到nacos
 * @param <T>
 */
public class NacosWritableDataSource<T> implements WritableDataSource<T>{

    private String dataId;
    private String groupId;
    private String nameSpace;
    private String serverAddr;
    private ConfigService configService;

    public NacosWritableDataSource(String dataId, String groupId, String nameSpace, String serverAddr) throws Exception {
        this.dataId = dataId;
        this.groupId = groupId;
        this.nameSpace = nameSpace;
        this.serverAddr = serverAddr;
        nacosConfigService();
    }

    public void nacosConfigService() throws Exception {
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.SERVER_ADDR,serverAddr);
        properties.setProperty(PropertyKeyConst.NAMESPACE,nameSpace);
        configService = ConfigFactory.createConfigService(properties);
    }


    @Override
    public void write(T t) throws Exception {
        configService.publishConfig(dataId,groupId, JSON.toJSONString(t));
    }

    @Override
    public void close() throws Exception {

    }
}
