package com.cp.demo.config;

import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.transport.util.WritableDataSourceRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.List;

@Configuration
public class NacosConfig {

    @Value("${nacos.sentinel.flowRuleDataId:}")
    private String flowRuleDataId;
    @Value("${nacos.sentinel.flowRuleGroupId:}")
    private String flowRuleGroupId;
    @Value("${nacos.sentinel.degradeRuleDataId:}")
    private String degradeRuleDataId;
    @Value("${nacos.sentinel.degradeRuleGroupId:}")
    private String degradeRuleGroupId;
    @Value("${nacos.address:}")
    private String address;
    @Value("${nacos.sentinel.nameSpaceId:}")
    private String nameSpaceId;
    @Value("${nacos.sentinel.groupId:}")
    private String groupId;

    /**
     * 流控规则推送
     * @return
     */
    @PostConstruct
    public void flowRuleNacosWritableDataSource(){
        if (!StringUtils.isEmpty(flowRuleDataId)){
            if (StringUtils.isEmpty(flowRuleGroupId) && StringUtils.isEmpty(groupId)) {
                throw new IllegalArgumentException("groupId is empty !");
            }
            final NacosWritableDataSource<List<FlowRule>> nacosWritableDataSource;
            try {
                nacosWritableDataSource = new NacosWritableDataSource<List<FlowRule>>(flowRuleDataId,flowRuleGroupId,nameSpaceId,address);
                WritableDataSourceRegistry.registerFlowDataSource(nacosWritableDataSource);
               
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
    }

    /**
     * 熔断规则推送
     * @return
     */
    @PostConstruct
    public void degradeNacosWritableDataSource(){
        if (!StringUtils.isEmpty(degradeRuleDataId)){
            if (StringUtils.isEmpty(degradeRuleGroupId) && StringUtils.isEmpty(groupId)) {
                throw new IllegalArgumentException("groupId is empty !");
            }
            final NacosWritableDataSource<List<DegradeRule>> nacosWritableDataSource;
            try {
                nacosWritableDataSource = new NacosWritableDataSource<List<DegradeRule>>(degradeRuleDataId,
                        StringUtils.isEmpty(degradeRuleGroupId)?groupId:degradeRuleGroupId,
                        nameSpaceId,
                        address);
                WritableDataSourceRegistry.registerDegradeDataSource(nacosWritableDataSource);
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
    }
}
