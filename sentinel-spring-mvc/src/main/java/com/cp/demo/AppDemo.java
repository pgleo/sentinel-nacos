package com.cp.demo;

import com.alibaba.csp.sentinel.datasource.ReadableDataSource;
import com.alibaba.csp.sentinel.datasource.nacos.NacosDataSource;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.nacos.api.PropertyKeyConst;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Properties;

/**
 * -Dcsp.sentinel.dashboard.server=127.0.0.1:8080   sentinel控制台地址
 * -Dproject.name=spring-webmvc-3   sentinel控制台展示的应用名
 * -Dcsp.sentinel.api.port=9092     数据更新时回调客户端的端口号，服务启动时自动创建对应端口socket
 */
@SpringBootApplication
@RestController()
public class AppDemo {
    // nacos server ip
    private static final String remoteAddress = "localhost:8848";
    // nacos group
    private static final String groupId = "sentinel_rule";
    // nacos dataId
    private static final String flow_dataId = "test-flow-rule";
    private static final String degrade_dataId = "test-degrade-rule";
    private static final String NACOS_NAMESPACE_ID = "7164284d-bfed-4648-8446-914a0784c662";

    public static void main(String[] args) {
        SpringApplication.run(AppDemo.class,args);
        initRule();
    }

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    //动态加载nacos的规则配置
    private static void initRule() {

        Properties properties = new Properties();
        properties.put(PropertyKeyConst.SERVER_ADDR, remoteAddress);
        properties.put(PropertyKeyConst.NAMESPACE, NACOS_NAMESPACE_ID);

        ReadableDataSource<String, List<FlowRule>> flowRuleDataSource = new NacosDataSource<>(properties, groupId, flow_dataId,
                source -> JSON.parseObject(source, new TypeReference<List<FlowRule>>() {
                }));
        FlowRuleManager.register2Property(flowRuleDataSource.getProperty());
        ReadableDataSource<String, List<DegradeRule>> DegradeRuleDataSource = new NacosDataSource<>(properties, groupId, degrade_dataId,
                source -> JSON.parseObject(source, new TypeReference<List<DegradeRule>>() {
                }));
        DegradeRuleManager.register2Property(DegradeRuleDataSource.getProperty());
    }
}
